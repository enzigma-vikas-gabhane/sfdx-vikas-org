@isTest
private class CandidateTriggerTest {
static List<Candidate_Vikas__c> lstTestCandidate = new List<Candidate_Vikas__c>();
static Job_Vikas__c job;
// Create  Candidates data for test class
@testSetup static void setup() {
    
    job = new Job_Vikas__c(Name='testJob',Active1__c=true,Job_Type__c='Developer',Number_of_Positions__c=4);
    insert job;
    // insert multiple candiates to test
    
    for(Integer i=0;i<20;i++) {
        lstTestCandidate.add(new Candidate_Vikas__c(Name = 'testCandidate'+i, First_Name__c='testFirst'+i,
                                                    Last_Name__c='testLast'+i,Status__c='Applied',Expected_Salary__c=2000,
                                                    Name__c=job.Id, Application_Date__c=Date.today().addDays(-7)));
    }
    Database.insert(lstTestCandidate);        
    
}

// test method to check Expected Salary field
@isTest static void testExpectedSalary(){
   
    
     	List<Job_Vikas__c> jobObjList  = [SELECT Id FROM Job_Vikas__c Limit 1];
       	Candidate_Vikas__c candidate = new Candidate_Vikas__c(First_Name__c = 'Test',Last_Name__c = 'Test',Country__c = 'India',State__c='Gujrat',Status__c='Applied',Email__c = 'test@gmail.com',Expected_Salary__c = null,Name__c = jobObjList[0].Id);
       	Database.SaveResult result = Database.insert(candidate, false);
       	System.assertEquals('Expected Salary field is missing',result.getErrors()[0].getMessage());
    	
        List<Candidate_Vikas__c> candList= [SELECT Expected_Salary__c FROM Candidate_Vikas__c  WHERE id IN: lstTestCandidate AND Expected_Salary__c = null];
    	System.assertEquals(0,candList.size());    
    
    
}
//test method to check weather job is active or not before applying
@isTest static void testActiveJobToApply(){
    
    
    
    	Job_Vikas__c Job = new Job_Vikas__c(Name='testJob',Active1__c=false,Job_Type__c='Developer',Number_of_Positions__c=4);
    	insert Job;
       Candidate_Vikas__c candidate = new Candidate_Vikas__c(First_Name__c = 'Test1',Last_Name__c = 'Test1',Country__c = 'India',State__c='Gujrat',Status__c='Applied',Email__c = 'gundaa@gmail.com',Expected_Salary__c = 300,Name__c = Job.Id);
       Database.SaveResult Result = Database.insert(candidate,false);
       System.assertEquals('This Job is not active. You can not apply for this job.',Result.getErrors()[0].getMessage());

    
    
}

@isTest static void checkApplicationDate(){

    	/*Candidate_Vikas__c candidate = new Candidate_Vikas__c( Application_Date__c=system.today(), First_Name__c = 'Test1',Last_Name__c = 'Test1',Country__c = 'India',State__c='Gujrat',Status__c='Applied',Email__c = 'gundaa@gmail.com',Expected_Salary__c = 300,Name__c = Job.Id);
       	Database.insert(candidate);
    	//insert candidate;
    	//candidate.Application_Date__c=candidate.CreatedDate.date();
    	//Database.SaveResult Result = Database.insert(candidate,false);
    	//update candidate;
    	//Test.stopTest();
    	
       	System.assertEquals(candidate.CreatedDate.date(), candidate.Application_Date__c);
		*/
    
    	List<Candidate_Vikas__c> lstCandidate = [SELECT id, Name, CreatedDate,Application_Date__c FROM Candidate_Vikas__c];
        //system.debug('candidate -'+lstCandidate);
    	List<Candidate_Vikas__c> lstNewCandidate = new List<Candidate_Vikas__c>();
    	for(Candidate_Vikas__c candidate : lstCandidate){
            system.debug('candidate -'+candidate);
        	if(candidate.Application_Date__c == candidate.CreatedDate.date())
            {
                lstNewCandidate.add(candidate);
            }
    	}
    	system.assertEquals(20, lstNewCandidate.size());
		
}
}