global class DeletePermissionSetPB {
    
    @InvocableMethod
    global static void DeletePermissionSetOfUser(List<Id> userId) {
        system.debug('in');
        delete [SELECT Id, AssigneeId from PermissionSetAssignment WHERE AssigneeId IN: userId];
    	system.debug('out');
        /* 
        string permSetId = [SELECT Id FROM PermissionSet WHERE Label = 'New Permission Set' LIMIT 1].Id;
        If (permSetId != null){
            delete [SELECT Id from PermissionSetAssignment WHERE PermissionSetId = : permSetId AND AssigneeId = : userId];
		 */
        
    }
    
}