@Istest(SeeAllData=true)
public class AccountManagerTest {
    @IsTest
    public static void testaccountmanager() {
        RestRequest request = new RestRequest();
        request.requestUri = 'https://mannharleen-dev-ed.my.salesforce.com/services/apexrest/Accounts/0015g000009ifXVAAY/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;
        String expectedAccountName = 'Burlington Textiles Corp of America';
		//system.debug('test account result = '+ AccountManager.getAccount()); 
        Account resultAccount = AccountManager.getAccount();
        system.assert(resultAccount != null);
        system.assertEquals(expectedAccountName, resultAccount.Name);
    }
   
}