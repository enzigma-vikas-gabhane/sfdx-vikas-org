public class CandidateTriggerHandler {
    
        
    public static void checkExpectedSalary(List<Candidate_Vikas__c> lstCandidate) {
        
       for(Candidate_Vikas__c candidate : lstCandidate)
        {
            if(candidate.Expected_Salary__c==null)
            {
                candidate.addError('Expected Salary field is missing');
                //return 'Expected Salary field is missing';
            }
        }     	   
    }
    
    
    public static void checkJobActive(List<Candidate_Vikas__c> lstCandidate){
       
        List<Id> lstJobId = new List<Id>();
        for(Candidate_Vikas__c candidate : lstCandidate) {
        	lstJobId.add(candidate.Name__c);
        }
     
         Map <Id, Job_Vikas__c> JobActive= new Map<Id, Job_Vikas__c>([SELECT id,Active1__c FROM Job_Vikas__c WHERE Id IN:lstJobId AND Active1__c=false]);

        for(Candidate_Vikas__c candidate : lstCandidate) {
            
            if(JobActive.get(candidate.Name__c) != null) {
                
				candidate.addError('This Job is not active. You can not apply for this job.');                
            }
        }     
        
        
    }
    
    
    public static void set_Application_Date(List<Candidate_Vikas__c> lstCandidate) 
    {
        
        List<Candidate_Vikas__c> lstNewCandidate= new List<Candidate_Vikas__c>();
       for(Candidate_Vikas__c candidateObj: lstCandidate){
           
           if(candidateObj.Application_Date__c != candidateObj.CreatedDate.date()){
               Candidate_Vikas__c newRecord = new Candidate_Vikas__c();
               newRecord.Id=candidateObj.Id;
               newRecord.Application_Date__c=candidateObj.CreatedDate.date();
               lstNewCandidate.add(newRecord);
               
           }           
       }
        if(!lstNewCandidate.isEmpty()){
            update lstNewCandidate;
        }
       
    }
      
   
    
}