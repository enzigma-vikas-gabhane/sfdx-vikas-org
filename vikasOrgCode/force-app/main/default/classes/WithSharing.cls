public with sharing class WithSharing {
   public List<Candidate_Vikas__c> candidateRecords{get; set;}
   public WithSharing(){
       candidateRecords=[select Salutation__c, First_Name__c, Last_Name__c, Full_Name__c, Email__c, Status__c, Job__c, Age__c, DOB__c, Application_Date__c, Country__c, State__c, Expected_Salary__c  from Candidate_Vikas__c limit 10];        
   }
}