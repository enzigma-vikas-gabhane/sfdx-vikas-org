import { LightningElement, track, api } from 'lwc';

export default class ChildC extends LightningElement {
    @track trackParam = "MyTrackParam";
    @api apiParam = "MyApiParam";
    nonReactiveParam = "MyNonReactiveParam";

    onClickFun(){
        this.trackParam = "MyTrackParam--11";
        this.apiParam = "MyApiParam--11";
        this.nonReactiveParam = "MyNonReactiveParam--11"
    }
}