trigger TriggerCandidateVikasAsync on Candidate_Vikas__c (before insert, before update) {
    
    if(Trigger.isBefore && Trigger.isUpdate) {
        List<Id> lstId= new List<Id>();
        for(Candidate_Vikas__c c: Trigger.new) {
            
            if(c.Status__c=='Hired') {
                
                lstId.add(c.Id);
            }
        }
        if(lstId.size()!=null)
            sendEmail.SendToCandidate(lstId);
        
    }

}